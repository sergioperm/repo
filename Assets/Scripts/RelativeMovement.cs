﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RelativeMovement : MonoBehaviour {

    [SerializeField] private Transform target;
    [SerializeField] private Text textLabel;

    public float rotSpeed = 15.0f;
    public float moveSpeed = 6.0f;
    public float walkSpeed = 1.1f;
    public float jumpSpeed = 15.0f;
    public float gravity = -9.8f;
    public float terminalVelocity = -10.0f;
    public float minFall = -1.5f;
    public float pushForce = 3.0f;

    private CharacterController _charController;
    private float _vertSpeed;

    private ControllerColliderHit _contact;

    private Animator _animator;

    // Use this for initialization
    void Start() {
        _charController = GetComponent<CharacterController>();
        _vertSpeed = minFall;
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        Vector3 movement = Vector3.zero;
        
        float horInput = Input.GetAxis("Horizontal");
        float verInput = Input.GetAxis("Vertical");
        bool walkInput = Input.GetKey(KeyCode.LeftShift);

        float speedMove = 0.0f;

        if (walkInput) {
            speedMove = walkSpeed;
        }
        else {
            speedMove = moveSpeed;
        }

        if (horInput != 0 || verInput != 0) {

                    
            movement.x = horInput * speedMove;
            movement.z = verInput * speedMove;
            movement = Vector3.ClampMagnitude(movement, speedMove);

            Quaternion temp = target.rotation;
            target.eulerAngles = new Vector3(0, target.eulerAngles.y, 0);
            movement = target.TransformDirection(movement);
            target.rotation = temp;

            Quaternion direction = Quaternion.LookRotation(movement);
            transform.rotation = Quaternion.Lerp(transform.rotation, direction, rotSpeed * Time.deltaTime);
        }

        _animator.SetFloat("speed", movement.sqrMagnitude);
        _animator.SetBool("walk", walkInput);

        bool hitGround = false;
        RaycastHit hit;

        if (_vertSpeed < 0 && Physics.Raycast(transform.position, Vector3.down, out hit)) {
            float check = (_charController.height + _charController.radius) / 1.9f;
            hitGround = hit.distance <= check;
        }

        if (hitGround) {
            if (Input.GetButtonDown("Jump")) {
                _vertSpeed = jumpSpeed;
            } else {
                _vertSpeed = minFall;
                _animator.SetBool("jumping", false);
            }
        } else {
            //no surface 
            _vertSpeed += gravity * 5 * Time.deltaTime;

            if (_vertSpeed < terminalVelocity) {
                _vertSpeed = terminalVelocity;
            }

            if (_contact != null) {
                _animator.SetBool("jumping", true);
            }

            if (_charController.isGrounded) {
                if (Vector3.Dot(movement, _contact.normal) < 0) {
                    movement = _contact.normal * speedMove;
                    //ya nihuya ne ponyal blyat
                } else {
                    movement += _contact.normal * speedMove;
                    //ya nihuya ne ponyal blyat
                }
            }
        }
        
        movement.y = _vertSpeed;
        movement *= Time.deltaTime;
        _charController.Move(movement);

        textLabel.text = walkInput.ToString();
       
    }

    void OnControllerColliderHit(ControllerColliderHit hit) {
        _contact = hit;

        Rigidbody body = hit.collider.attachedRigidbody;
        if (body != null && !body.isKinematic) {
            body.velocity = hit.moveDirection * pushForce;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorOpenDevice : MonoBehaviour {

    [SerializeField] private Vector3 dPos;
    [SerializeField] private Text textLabel;

    private bool _open;
    private bool animInProgress;

    enum state {
        close,
        open,
        stay
    }

    private state currentState;

    public void Operate() {
        if (_open) {
            CloseDoor();
        } else {
            OpenDoor();
        }
    }

    public void Activate() {
        OpenDoor();
    }

    public void Deactivate() {
        CloseDoor();
    }

    private void OpenDoor() {

        currentState = state.open;

        if (!_open && !animInProgress) {
            Vector3 pos = transform.position + dPos;

            iTween.MoveTo(gameObject, pos, 2.0f);

            _open = !_open;
            currentState = state.stay;
        }
    }

    private void CloseDoor() {

        currentState = state.close;

        if (_open && !animInProgress) {
            Vector3 pos = transform.position - dPos;

            iTween.MoveTo(gameObject, pos, 2.0f);

            _open = !_open;
            currentState = state.stay;
        }
    }
	
	// Update is called once per frame
	void Update () {

        animInProgress = !(Mathf.Approximately(transform.position.y, -1.4f) || Mathf.Approximately(transform.position.y, 1.5f));

        textLabel.text = animInProgress.ToString();

        if (!animInProgress) {
            if (currentState == state.open) {
                OpenDoor();
            } else if (currentState == state.close) {
                CloseDoor();
            }
        }

    }
}

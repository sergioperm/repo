﻿
public interface IGamemanager {
    
    ManagerStatus status { get; }

    void StartUp();

}

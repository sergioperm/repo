﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBeamDoor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Activate() {
        MeshRenderer mesh = gameObject.GetComponent<MeshRenderer>();

        mesh.material.color = new Color(0.529f, 1f, 0.708f, 0.6f);

    }

    public void Deactivate() {
        MeshRenderer mesh = gameObject.GetComponent<MeshRenderer>();

        mesh.material.color = new Color(0.529f, 0.766f, 1f, 0.6f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrbitCamera : MonoBehaviour {

    [SerializeField] private Transform target;
    
    public float rotSpeed = 1.5f;

    private float _rotY;
    private float _rotX;

    private Vector3 _offset;
    private float distance;

    private Vector3 position;

    // Use this for initialization
    void Start () {
        _rotY = transform.eulerAngles.y;
        _rotX = transform.eulerAngles.x;
        _offset = target.position - transform.position;
	}

    private void LateUpdate() {
        
        float mouseXInput = Input.GetAxis("Mouse X");
        float mouseYInput = -Input.GetAxis("Mouse Y");

        _rotY += mouseXInput * rotSpeed * 3;
        _rotX += mouseYInput * rotSpeed * 3;
       
        _rotX = Mathf.Clamp(_rotX, -40.0f, 30.0f);

        Quaternion rotation = Quaternion.Euler(_rotX, _rotY, 0);
        transform.position = target.position - (rotation * _offset);

        transform.LookAt(target);
    }

    public static float ZoomLimit(float dist, float min, float max) {

        if (dist < min)
            dist = min;

        if (dist > max)
            dist = max;

        return dist;

    }

   
}
